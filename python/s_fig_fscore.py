# -*- coding: utf-8 -*-
# vim: set tabstop=4
# s_O2_ROCcurve.py
#!/usr/bin/env python3
""" The script plots the ROC curve of the dissolved oxygen sensors."""

# Copyright (C) 2018 Juan Pablo Carbajal
# Copyright (C) 2018 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

#' % Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
#' % Mariane Yvonne Schneider <myschneider@meiru.ch>
#' % 05.07.2018

import platform
import pickle

import numpy as np

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.colors import LightSource

from scipy.interpolate import griddata

from SBRdata.utils import fscore

matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'
matplotlib.rcParams.update({'font.size': 12})
if platform.system() != 'Windows':
    matplotlib.rcParams['text.usetex'] = True
else:
    pass

if __name__ == '__main__':
    import sys
    sensortype = str(sys.argv[1])
    inputfile = '../data/%s_Tables.pkl'%sensortype

    print ('Reading contingency tables to %s ...' %inputfile)
    with open(inputfile, 'rb') as fid:
        TFtable = pickle.load(fid)
        params = pickle.load(fid)
        param_names = pickle.load(fid)

    FSCORES = dict().fromkeys(TFtable.keys())
    betas = np.array([0.5, 1, 2])
    for sname in FSCORES:
        FSCORES[sname] = np.zeros((len(TFtable[sname]), len(betas)))
        for i,table in enumerate(TFtable[sname]):
            FSCORES[sname][i,:] = [fscore(table, b**2) for b in betas]

    dim = len(params)
    if dim == 2:
        # Illuminate the scene from the northwest
        ls = LightSource(azdeg=315, altdeg=65)
        orig = 'lower'
        ve = 1.0
        cmap = plt.cm.gist_earth

        # Prepare unifor grid fr plotting
        X0, Y0 = np.meshgrid(params[0], params[1])
        x = np.linspace(params[0].min(), params[0].max(), 80)
        y = np.linspace(params[1].min(), params[1].max(), 80)
        X, Y = np.meshgrid(x, y)
        extent = [X.min(), X.max(), Y.min(), Y.max()]

        k = 2
        vals = [(np.argmax(v[:,k]), v[:,k].max()) for v in FSCORES.values()]
        idx, zM = zip(*vals)
        xM = X0.ravel()[np.array(idx)]
        yM = Y0.ravel()[np.array(idx)]
        for x,y,z in zip(xM,yM,zM):
            print ('Max F%.1f-score: %.2f@(%.2f, %.2f)'%(betas[k], z, x, y))

        for j, beta in enumerate(betas):
            # get max and min levels
            maxlvl = np.max([v[:,j].max() for v in FSCORES.values()])
            minlvl = np.min([v[:,j].min() for v in FSCORES.values()])
            fig, axes = plt.subplots(1, len(FSCORES), sharey='all', sharex='all')
            i = -1
            for sname, score in FSCORES.items():
                i += 1
                # Inteprolate data on uniform grid
                Z = griddata(np.stack([X0.ravel(), Y0.ravel()],axis=1), score[:,j],
                    np.stack([X.ravel(), Y.ravel()],axis=1))
                Z = np.reshape(Z, X.shape)

                ax = axes[i]
                #ax.pcolor(X, Y, Z, cmap='summer', vmax=maxlvl, vmin=minlvl, n)
                # Shade with lights t higlight topography
                #rgb = ls.shade(Z, cmap=cmap, blend_mode='hsv', vmax=maxlvl,
                #    vmin=minlvl, vert_exag=ve)
                #ax.imshow(rgb, aspect='auto', origin=orig,
                #    extent=extent, interpolation='sinc')

                # plot contours
                levels = np.linspace(0.5, maxlvl, 20)
                ax.contourf(X, Y, Z, levels, cmap="RdBu_r")
                levels = np.linspace(0.5, maxlvl-0.01, 15)
                CS = ax.contour(X, Y, Z, levels, colors='k', linewidths=0.5,
                    extent=extent, origin=orig)
                ax.clabel(CS, fmt='%.3f', inline=True, fontsize=10)

                ax.set_title(sname)
                ax.set_xlabel(param_names[0])
                ax.set_xlim(1, 5)
                if i == 0:
                    ax.set_ylabel(param_names[1])

            plt.suptitle('F-score(%.1f)'%beta)
            if j < 2:
                plt.show(block=False)
            else:
                plt.show()

    elif dim == 1:
        fig, axes = plt.subplots(1, len(FSCORES), sharey='all', sharex='all')
        i = -1
        for sname, score in FSCORES.items():
            i +=1
            ax = axes[i]
            for j, beta in enumerate(betas):
                ax.plot(params[0], score)
                ax.set_xlim(params[0].min(), 7)
                ax.set_title(sname)
                ax.set_xlabel(param_names[0])
                # TODO print legend with F-score type outsid eplots wihtout deforming
        plt.show()
