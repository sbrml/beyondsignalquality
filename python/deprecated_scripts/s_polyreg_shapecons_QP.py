# vim: set tabstop=4
# s_polyreg_shapecons_QP.py
#!/usr/bin/env python3
"""Regression using orthogonal polynomaisl with shape constraints"""

__author__ = "Juan Pablo Carbajal"
__copyright__ = "Copyright (C) 2018 - Juan Pablo Carbajal"
__credits__ = ["Juan Pablo Carbajal"]
__license__ = """This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>."""
__version__ = "0.0.1"
__maintainer__ = "Juan Pablo Carbajal"
__email__ = "ajuanpi+dev@gmailcom"
__status__ = "Development"


import numpy as np

from numpy import polynomial as poly
from scipy import optimize

def vandermonde (basis, x, deriv=0):
    V = np.zeros((len(x),len(basis)))
    dV = None
    if deriv > 0:
        dV = np.zeros((len(x),len(basis), deriv))
    for i,b in enumerate(basis):
        V[:,i] = b(x)
        if deriv > 0:
            for j in range(deriv):
                dV[:,i,j] = b.deriv(j+1)(x)

    return V, dV

to = np.linspace(0,1,50)
N = len(to)
yo = np.cos(np.pi*to) + 1*np.exp(-(to-0.75)**2/2/0.03**2)
yom = yo.mean()

polyclass = poly.Legendre
polyopt={'domain':[to.min(), to.max()],'window':[-1,1]}

n = 7
B = []
for i in range(n):
    B.append(polyclass.basis(i,**polyopt))

V, dV = vandermonde(B, to, deriv=2)
d2V = dV[:,:,1]
dV = dV[:,:,0]

def weight(r, lx):
    r = (r - r.min()) / (r.max() - r.min())
    u = r/lx

    W = np.exp(-u)

    W = np.diag(W)

    return W

def loss(x, lx, ld):
    res = yo - V.dot(x)
    dy = d2V.dot(x)
    W = weight(res + (yo -yo.min()),lx)
    Loss = 0.5 * res.T.dot(W.dot(res)) + ld*dy.T.dot(dy)
    return Loss

cons = {'type':'ineq',
        'fun':lambda x: -dV.dot(x),
        'jac':lambda x: -dV}

opt = {'disp':False}

x0 = np.zeros(V.shape[1])
lx = 2e-1
ld = 1e-8
if not ld:
    fun = np.inf
    for ld_ in np.logspace(-8,0,10):
        sol_ = optimize.minimize(loss, x0, args=(lx,ld_), method='SLSQP', \
            options=opt, constraints=cons)
        if sol_['fun'] < fun:
            sol = optimize.OptimizeResult(sol_)
            fun = sol['fun']
            ld = [ld_][0]
else:
    sol = optimize.minimize(loss, x0, args=(lx,ld), method='SLSQP', \
        options=opt, constraints=cons)

print(sol)
tol = np.abs(sol['x']).max() * 1e-2
py = polyclass(sol['x'],**polyopt).trim(tol)

t = np.linspace(to.min(), to.max(), 2*N)

from matplotlib import pyplot as plt
plt.ion()
plt.figure(1)
plt.clf()
plt.subplot(2,1,1)
plt.plot(to,yo,'o')
plt.plot(t,py(t),'-')
plt.grid()

plt.subplot(2,1,2)
plt.plot(t,py.deriv()(t),'-')
plt.grid()
