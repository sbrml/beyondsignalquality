# -*- coding: utf-8 -*-
# vim: set tabstop=4
# s_preprocess_pH.py
#!/usr/bin/env python3

""" Show the results of features on the pH signal """

# vim: set tabstop=4
# s_preprocess_pH.py
#!/usr/bin/env python3

# Copyright (C) 2018 Maraine Yvonne Schneider
# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

from SBRdata.dataparser import SensorData
from pH_features import aeration_valley, aeration_climax

data = SensorData('../data/180109_AI4_MW.csv')
data.pH = data.interp_nan()

# Select aeration phase
idx = np.nonzero(data.phase==4)[0]
t = data.time[idx]
dt = t[1] - t[0]
fNyq = 1 / (2 * dt)
T = t[-1]-t[0]
pH = data.pH[:,idx]

"""Options for aeration_valley/climax: smoother and finder"""
## Finder
w = np.int(2*np.floor(0.06/dt/2)+1)
# default find peaks: realtive maxima
fp_valley_opt = {'order':np.copy(w)}
## Smoother
# default smoother: butter 3rd order 
s_valley_opt = {'freq':10}

## Finder
w = np.int(2*np.floor(0.02/dt/2)+1)
# default find peaks: realtive maxima
#fp_climax_opt = {'order':w2}
# greater or equal
fp_climax_opt = {'finder':signal.argrelextrema, 'comparator':np.greater_equal, 
    'order':np.copy(w)}
## Smoother
# default smoother: butter order 3
s_climax_opt = {'freq':20}
# order filter
#s_climax_opt = {'smoother':signal.order_filter, 'domain':np.ones(w), 'rank':int(np.round(w/2))}

#plt.ion()
for i,s in enumerate(pH):
    if True:
        print('Cycle %d'%i)
        m, s_valley = aeration_valley(t, s, \
            smooth_opt=s_valley_opt, findpeaks_opt=fp_valley_opt)
        M, s_climax = aeration_climax(t, s, valley=m, \
            smooth_opt=s_climax_opt, findpeaks_opt=fp_climax_opt)

        plt.figure(1)
        plt.clf()
        plt.plot(t, s, label='signal')
        plt.autoscale(enable=False, axis='y', tight=True)
        plt.plot(t, s_valley, color='g', label='smoothed valley signal')
        plt.plot(t, s_climax, color='r', label='smoothed climax signal')
        if m:
            plt.plot(*m, 'go', label='valley')
        if M:
            plt.plot(*M, 'ro', label='first peak')
        plt.xlabel('time')
        plt.ylabel('pH')
        plt.legend()
        plt.autoscale(enable=True, axis='x', tight=True)

        # plot PSD
        '''
        plt.figure(2)
        plt.clf()
        f, Pwelch_spec = signal.welch(s, 1/dt, scaling='spectrum')
        plt.semilogy(f, Pwelch_spec)
        plt.xlabel('frequency [Hz]')
        plt.ylabel('PSD')
        plt.grid()
        '''
        plt.show()
#        plt.waitforbuttonpress()
