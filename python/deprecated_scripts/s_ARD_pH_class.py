# vim: set tabstop=4
# s_ARDfeatures_pH.py
#!/usr/bin/env python3
'''
# WARNING: this program produces on certain Windows 7 version a power cut!
# WARNING: blackscreen on Windows 7

Automatic relevance determination based on pH signal. Compares results of
maintained and unmaintained Signals.
'''
'''
 Copyright (C) 2018 Juan Pablo Carbajal
 Copyright (C) 2018 Maraine Yvonne Schneider

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Mariane Yvonne Schneider <m.sammel@meiru.ch>

############
## Imports
# built-ins
from SBRdata.dataparser import SensorData, IOData

# 3rd party
import numpy as np
import matplotlib.pyplot as plt
import platform
import matplotlib
if platform.system() is not 'Windows':
    matplotlib.rcParams['text.usetex'] = True
else:
    pass
matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'

from sklearn.linear_model import ARDRegression
from sklearn.metrics import mean_squared_error, r2_score
from scipy.signal import argrelextrema
from scipy import signal

# TODO: do this OS robust
datapath = '../data/'
files_ = {'pH':'pH CPS11D.1.csv'}
filename_ = datapath + files_['pH']

## Load data and verify integrity of outputs
pHData_ = SensorData(filename_)
pH_ = pHData_.interp_nan()
Ydata_ = IOData(filename_)

# filter NA values in output and discard the pH cycles without io measurements
NH4_ = Ydata_.NH4[:,1]
nonan_ = np.logical_not(np.isnan(NH4_))
NH4_ = NH4_[nonan_]
pH_ = pH_[nonan_,:]

# if this step is included only the NH4+N concentrations above 0.3 are included
above_ = np.greater(NH4_,0.5)
NH4_ = NH4_[above_]
pH_ = pH_[above_,:]

if 'reg' not in vars():
    # Regression with ARD and check the weight of eacht time step
    reg_ = ARDRegression (n_iter=int(100), compute_score=True)
    reg_.fit(pH_, NH4_)

    # Order features with decreasing weight
    o_ = np.argsort(np.abs(reg_.coef_))[::-1]
    idx_ = o_[:16]

# Regress with selected Features
reg_s_ = ARDRegression (n_iter=int(300))
reg_s_.fit(pH_[:,idx_], NH4_)
y_pred_ = reg_s_.predict(pH_[:,idx_])

print("Mean squared error: %.2f for the UNmaintained sensor."
      % mean_squared_error(NH4_, y_pred_))
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f for the UNmaintained sensor.'
      % r2_score(NH4_, y_pred_))

files = {'pH':'AI4_MW.csv'}
filename = datapath + files['pH']

## Load data and verify integrity of outputs
pHData = SensorData(filename)
pH = pHData.interp_nan()
Ydata = IOData(filename)

# filter NA values in output and discard the pH cycles without io measurements
NH4 = Ydata.NH4[:,1]
nonan = np.logical_not(np.isnan(NH4))
NH4 = NH4[nonan]
pH = pH[nonan,:]

# if this step is included only the NH4+N concentrations above 0.3 are included
above = np.greater(NH4,0.5)
NH4 = NH4[above]
pH = pH[above,:]

if 'reg' not in vars():
    # Regression with ARD and check the weight of eacht time step
    reg = ARDRegression (n_iter=int(100), compute_score=True)
    reg.fit(pH, NH4)

    # Order features with decreasing weight
    o = np.argsort(np.abs(reg.coef_))[::-1]
    idx = o[:16]

# Regress with selected Features
reg_s = ARDRegression (n_iter=int(300))
reg_s.fit(pH[:,idx], NH4)
y_pred = reg_s.predict(pH[:,idx])

print("Mean squared error: %.2f for the maintained sensor."
      % mean_squared_error(NH4, y_pred))
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f for the maintained sensor.'
      % r2_score(NH4, y_pred))

n = 1
fontsize = 14
#+ caption= "ARD feature selection"
plt.ion()
plt.figure(1)
plt.clf()
plt.subplot(2,1,1)
plt.plot(pHData.time, reg.coef_)
plt.ylabel('Weight of the model')
plt.subplot(2,1,2)

s = np.abs(reg.coef_[idx])
s = 36 * s / s.max() + 16
plt.scatter(pHData.time[idx], np.mean(pH[:,idx],axis=0), s=s, alpha=0.9)
#plt.autoscale(enable=False, axis='y', tight=True)
plt.ylim(0,3)
plt.autoscale(enable=False, axis='x', tight=True)
plt.plot(pHData.time, pH.T,'-', linewidth=0.5)
plt.ylabel('pH')
plt.show()

#+ caption= "Regression results."
plt.figure(2)
plt.clf()
plt.plot(NH4,'o', label='data')
plt.plot(y_pred,'x', label='pred.')
plt.ylabel('NH4')
plt.xlabel('Cycle')
plt.legend()
plt.show()

#+ caption= "Regression results."
plt.figure(3)
plt.clf()
plt.plot(NH4,y_pred,'o')
plt.plot(NH4_,y_pred_, 'x')
plt.plot(0, 50,c="black",linewidth=1,linestyle='--',zorder=0)
plt.ylabel('NH4measured')
plt.xlabel('NH4predicted')
plt.ylim(0,50)
plt.xlim(0,50)
plt.legend()
plt.show()

plt.figure(4)
plt.clf()
plt.plot(NH4,'go', label='data')
plt.plot(y_pred_, 'bx', label='pred. unmaint.')
plt.plot(y_pred, 'rx', label='pred. maint.')
plt.ylabel('NH4+N')
plt.xlabel('Cycle')
plt.ylim(0,50)
plt.xlim(0,50)
plt.legend()
plt.show()
