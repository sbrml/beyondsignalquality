# -*- coding: utf-8 -*-
# vim: set tabstop=4
# dataparser.py
#!/usr/bin/env python3

# Copyright (C) 2018 Juan Pablo Carbajal
# Copyright (C) 2018 Maraine Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

#' % Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
#' % Maraine Yvonne Schneider <myschneider@meiru.ch>
#' % 20.04.2018

"""Parser to read data from csv files"""
from csv import reader

from datetime import datetime

import numpy as np

from scipy.interpolate import interp1d

class BaseData:
    """A general data container reading from CSV files"""

    def __init__(self, filename, delimiter=';'):

        self.filename = filename
        self._delimiter = delimiter
        self._raw_data = self.retrieve_data()

        # Names of rows
        self.row_name = [x[0] for x in self._raw_data]
        self._row0 = self.row_name.index('cycle')
        # Names and columns of outputs
        cycle_row = self._raw_data[self._row0]
        self.out_name = [x for x in cycle_row[1:] if x != '']
        self._col0_out = cycle_row.index(self.out_name[0])

        # Number of data rows
        self.ndatarows = len(self.row_name) - self.row_name.index('cycle') + 1

    def retrieve_data(self):
        """Read the whole file into memory"""
        with open(self.filename, 'rt') as fid:
            raw_data = list(reader(fid, delimiter=self._delimiter))
        return raw_data

    def cycle_names(self, cycle=None):
        """ Return the name of the cycle

            The input should be a number, if none is given
            all names are returned
        """
        if cycle is None:
            return self.row_name[(self._row0 + 1):]
        elif cycle >= 0:
            return self.row_name[self._row0 + 1 + cycle]
        else:
            raise ValueError('cycle number should be positive')

class SensorData(BaseData):
    """A container for the sensor signals
        O2:
        pH:
        ORP:
    """

    def __init__(self, filename, *, delimiter=';'):
        super().__init__(filename, delimiter)

        # Number of data points per row
        # first column is label and index('') identifies the last row containing
        # cycle data. Therefore it returns the amount of datapoints.
        self.nT = self._raw_data[0].index('') - 1
        # Time vector from 0 to 1 for all entries in row 0 FIXME when I delete
        # later on does that still work for the time steps? (Mariane)
        self.time = np.linspace(0, 1, self.nT)
        # Number of sensor signals
        self.ndatarows = len(self.row_name) - self.row_name.index('cycle') + 1
        # Phase array excluding the string 'cycle'
        self.phase = np.array(self._raw_data[0][1:self.nT+1], dtype=np.int8)
        # sensor signals matrix
        self.sensor = self._convert()

        """ Get information from cycle datefilename

            It assumes that the filename follows the naming pattern:

            <yyyymmdd>_<hhmm>_<cycle number>.csv

            where
              * <yyyymmdd> is the year (4 digits), month (2 digits), and <dd>
                day number (2 digits)
              * <hhmm> i the starting time of the cycle in hh (24h based
                hour of the day) and mm (minutes after the hour).
              * <cycle number> is a 6 digits unique id of the cycle.
        """
        self._datestr = []
        self._timestr = []
        self._cycleid = []
        for name in self.cycle_names():
            datestr, timestr, cycleid_ext = name.split('_')
            self._datestr.append(datestr)
            self._timestr.append(timestr.split('.')[0])
            self._cycleid.append(cycleid_ext.split('.')[0])
        self.datetime = self.cycles_datetime()

    def _convert(self):
        """Convert sensor data into float or None"""
        n0 = self._row0
        _list = []
        for strx in self._raw_data[n0+1:]:
            x = [x if x != '' else None for x in strx[1:self.nT+1]]
            _list.append(x)
        return np.array(_list, np.float64)

    def phase_indicator(self, phases):
        """ Retur bolean variable indicating the segment of the data
        that correspond to the given phases
        """
        try:
            tf = np.array([False] * self.time.size)
            for p in phases:
                tf = tf | (self.phase == p)
        except TypeError:
            tf = (self.phase == phases)
        return tf

    def phase_index(self, phases):
        """Return the indexes into the data correspoding to the given phase"""
        return np.nonzero(self.phase_indicator(phases))[0]

    def interp_nan(self, *, phase):
        """ Replace nan values in each phase with interpolated values

            The optinal keyword phase is used to pre-select a phase of the cycle
            for interpolation.
        """
        idx = self.phase_index(phase)
        t = self.time[idx]
        s = self.sensor[:, idx].copy()
        nan = np.isnan(s)
        not_nan = np.logical_not(nan)

        for i, cycle in enumerate(self.sensor[:, idx]):
            if any(nan[i, :]):
                n = nan[i, :]
                nn = not_nan[i, :]
                if np.sum(nn) == 1:  # single not nan value: use it
                    s[i, n] = cycle[nn]
                elif np.sum(nn) > 1: # many not nan values: interpolate
                    s[i, n] = interp1d(t[nn], cycle[nn], \
                        fill_value="extrapolate", assume_sorted=True)(t[n])
                else: # none not nan values: error
                    raise ValueError( \
                        'All signal values (N={}, cycle={}) are nan'\
                        .format(np.sum(n), i))
        return s

    def cycles_datetime(self):
        """ converts date and time strings to datetime object
            Check __init__ for expected format of those strings
        """
        dayhour = []
        for date, time in zip(self._datestr, self._timestr):
            day = datetime.strptime(date, "%Y%m%d").date()
            hour = datetime.strptime(time, "%H%M").time()
            dayhour.append(datetime.combine(day, hour))

        return dayhour

    def completion_phase(self, phase, *, aspercent=False):
        """ Return the completion of a given phase

            The completion goes from 0 (start) to 1 (end).
            If the optional argument aspercent is given the
            completion is given in percent, i.e. 0 to 100 perent
        """
        t = self.time[self.phase == phase]
        t = (t - t[0]) / (t[-1] - t[0])
        if aspercent:
            t = t * 100
        return t


class IOData(BaseData):
    """A container for the input-output values"""
    def __init__(self, filename, *, delimiter=';'):
        super().__init__(filename, delimiter)

        c = self._col0_out+5
        r = self._row0+19
        self._raw_data[r][c] = self._raw_data[r][c][:-1]
        # IO values
        self.IO = self._convert()

    def __getattr__(self, name):
        """Return any valid input/output value

            If name is not in self.out_name an error is raise.
            If the value has affluent (inf) and effluent (eff)
            values then both are returned in the 1st and 2nd column
            respectively.
        """
        names = [x.split()[0] for x in self.out_name]
        if name not in names:
            raise ValueError('%s is not an input/output value' % name)
        try:
            value = np.atleast_2d(self.IO[:, self.out_name.index(name)]).T
        except ValueError:
            ci = self.out_name.index('%s inf' % name)
            co = self.out_name.index('%s eff' % name)
            value = self.IO[:, [ci, co]]
        return value

    def _convert(self):
        """ Convert IO data into float or None

            If a value in the data starts with '<'
            it is converted to 0.0.
        """
        r0 = self._row0
        c0 = self._col0_out
        IO_list = []
        for strx in self._raw_data[r0+1:]:
            x = []
            for c in strx[c0:]:
                if c:
                    if c[0] == '<':
                        c = '0.0'
                else:
                    c = None
                x.append(c)
            IO_list.append(x)
        return np.array(IO_list, np.float64)

    def filter_missing(self, name, flowdir=''):
        """ Return data with missing values removed

            if the optional argument flowdir is given it must be
            'in' or 'out' to indicate that only input or output values
            are desired.
        """
        value = self.__getattr__(name)
        finite = np.isfinite(value)
        if finite.shape[1] > 1:
            finite = finite[:, 0] & finite[:, 1]

        if value.shape[1] > 1:
            flowdir = flowdir.lower()
            if 'out' in flowdir:
                value = value[:, 1]
            elif 'in' in flowdir:
                value = value[:, 0]

        return value[finite], finite
